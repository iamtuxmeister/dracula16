#!/usr/bin/env bash
gsettings set io.elementary.terminal.settings palette "#000000:#b6433e:#38b661:#b6ba66:#8669b1:#ae5889:#69b3c2:#c7c7c7:#686868:#ff6e67:#5af78e:#f4f99d:#caa9fa:#ff92d0:#9aedfe:#ffffff"
gsettings set io.elementary.terminal.settings background "#282a36"
gsettings set io.elementary.terminal.settings foreground "#ebebeb"
gsettings set io.elementary.terminal.settings cursor-color "#ebebeb"
